# Suis-je en sécurité sur Internet&nbsp;?

En matière de lutte contre les malveillances informatiques, les pratiques des utilisateurs se sont plutôt améliorées ces dernières années. Cela fait suite en particulier aux révélations successives sur la surveillance des firmes et l'affaire du programme de surveillance de la NSA ([PRISM](https://fr.wikipedia.org/wiki/PRISM_(programme_de_surveillance)))  révélé par Edward Snowden. La confiance des utilisateurs envers les plates-formes de service et les logiciels en général a largement baissé, ce qui a causé en retour un effet sur le marché&nbsp;: la recherche individuelle de solutions sécurisées et une modification des stratégies sécuritaires par les entreprises et collectivités. Cela s'est même concrétisé par l'ouverture d'une économie de la confiance avec, comme paradoxe, le fait que les mêmes firmes impliquées dans l'affaire PRISM cherchent à proposer (démontrer) des solutions visant à garantir d'une manière ou d'une autre la confidentialité des utilisateurs. Mais ces derniers ne sont pas dupes.

Évidemment, ces stratégies n'auraient aucune raison d'être poursuivies si, d'un autre côté, les attaques malveillantes ne se multipliaient pas, menées tant par de puissantes organisations du crime comme par des gagne-petits au pouvoir de nuisance multiplié par les possibilités informatiques. En effet, nombre d'entreprises et collectivités de toutes tailles font régulièrement l'objet de tentatives de [fishing](https://fr.wikipedia.org/wiki/Hame%C3%A7onnage), attaques par [déni de service](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service) et de [ransomware](https://fr.wikipedia.org/wiki/Ransomware). Les individus en sont aussi les victimes, très régulièrement, qu'il s'agisse d'arnaques ou de virus. Les pièces jointes dans les courriels sont trop vite ouvertes…

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Prism-Break</strong></p>

En matière de protection individuelle, le site internet multilingue <a href="https://prism-break.org/fr/">Prism-Break</a> est un bel exemple de mise à disposition de solutions logicielles alternatives pour les utilisateurs qui comprennent de cette manière combien le logiciel libre est important dans ce domaine, à la fois pour se protéger des malveillances mais aussi assurer la confidentialité, la vie privée, face à n'importe quelles intrusions.

</div>

Qu'entend-on par sécurité&nbsp;? En fait, il s'agit de trois choses&nbsp;:

1. *la disponibilité des données*&nbsp;: pouvoir accéder à ses données lorsqu'on le souhaite et ne pas toujours dépendre d'un tiers, fût-il de confiance&nbsp;;
2. *l'intégrité des données*&nbsp;: il faut que les données ne soient pas modifiées sans autorisation&nbsp;;
3. *la confidentialité*&nbsp;: les données qui m'appartiennent ainsi que les informations que l'on pourrait inférer de mes usages ne doivent pas être accessibles sans autorisation.

Ces trois domaines concernent respectivement&nbsp;:

1. *l'usage de services sur Internet*, en particulier les services liés à la messagerie, au *cloud* et aux réseaux sociaux. À la différence de services centralisés (comme Facebook), la décentralisation de ce type de services (comme le réseau Diaspora*) permet de réduire les risques de rupture (ou de censure).  Il faut donc bien choisir les services que l'on utilise en connaissance de cause. Pour ce qui concerne l'usage informatique en local, la bonne santé de votre machine, les mises à jour des logiciels, et tout un ensemble de pratiques rigoureuses vous permettront d'assurer la disponibilité de vos données&nbsp;;
2. *la neutralité des réseaux*&nbsp;: le transfert de données ne doit pas modifier les données. Que diriez-vous si votre facteur ouvrait votre courrier, faisait une copie de la lettre pour réduire sa taille (au risque de perdre de la lisibilité ou même en coupant des parties), prenait une enveloppe plus petite et vous donnait le résultat&nbsp;? C'est pourtant ce que certains fournisseurs d'accès souhaiteraient faire au nom de l'économie de bande passante. Bien sûr, l'intégrité de vos données est plus directement menacée par les actes de malveillance (piratage). Pour s'en prémunir, il faut là encore employer des logiciels sûrs, lutter contre les vulnérabilités aux virus, ne pas utiliser des services qui modifient les contenus ou se les approprient&nbsp;;
3. *les risques liés à la surveillance*, qu'il s'agisse de votre voisin, de firmes ou d'États. Ce genre de pratique ne met pas seulement en jeu les intimités numériques, car si nous nous sentons surveillés, nous censurons automatiquement nos propos. Dès lors, on comprend pourquoi la liberté d'expression est l'une des premières libertés menacées par la surveillance sur Internet.

Toutes ces questions peuvent être regroupées en une seule&nbsp;: *quelle confiance puis-je accorder aux dispositifs numériques, aux logiciels et aux services que j'utilise&nbsp;?* Cette confiance ne peut pas être aveugle. Par exemple, même si vous avez *a priori* confiance envers votre fournisseur d'accès, il est probable que vous seriez plus serein si les courriels que vous envoyez à votre banquier étaient chiffrés de manière à ce que seul votre banquier puisse les lire.

La confiance nécessite donc à la fois une expertise et des pratiques. Or tout le monde n'a pas l'expertise suffisante pour évaluer la confiance envers les logiciels et les services. C'est pour cela que l'utilisation d'outils libres, dont le code source est ouvert, permet de faire reposer cette expertise sur un collectif nombreux de pairs plutôt que sur une personne, entreprise ou institution. Il en va de même pour les services en ligne, qui peuvent reposer sur des solutions de logiciels libres. Quant aux pratiques, ce sont d'elles que nous allons parler dans ce chapitre&nbsp;: la sécurisation des dispositifs, le chiffrement, la protection des accès.


<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Rejoignez la SecNumacadémie</strong></p>

En France, l'Agence Nationale de la Sécurité des Systèmes d'Information (ANSSI) a lancé un MOOC (*massive open online course*) ouvert à tous dans le but de sensibiliser à la sécurité informatique. Très pédagogique et progressif, ce MOOC est largement conseillé pour prendre la mesure des enjeux de la sécurité et des bonnes pratiques, au quotidien comme dans la vie professionnelle. Une adresse  : [secnumacademie.gouv.fr](https://www.secnumacademie.gouv.fr/).

</div>




## Protéger mes dispositifs



Qu'il s'agisse de votre ordinateur ou de votre smartphone, vous ne souhaitez pas que n'importe qui puisse accéder à vos données. Si les systèmes d'exploitation insistent bien souvent sur la création d'un mot de passe garantissant l'ouverture des sessions, trop nombreux sont les utilisateurs qui passent outre cette recommandation. Ce n'est cependant qu'une faille parmi d'autres contre les malveillances dont vous pouvez faire l'objet.

### Sessions et profils d'utilisateurs

Oui, il est pénible de devoir entrer un mot de passe à chaque fois que l'on souhaite accéder à sa machine après un temps d'inactivité. Mais il faut avouer que le prix est peu cher payé pour un premier niveau élémentaire de protection. Qui a perdu son téléphone portable sans sécurisation de la session… n'a plus qu'à trouver très vite un autre moyen de se rendre sur les différents services utilisés pour en changer les accès, tout en espérant que le voleur n'ira pas trop fouiller dans les photos et autres documents stockés dans la mémoire.

Toutefois, si vous avez perdu votre ordinateur, tablette, ou smartphone, avec ou sans mot de passe, vous avez de toute façon un grand intérêt à changer vos autres accès&nbsp;: quel que soit le système d'exploitation, les mots de passe de session, voire les mots de passe d'administration, sont en général assez rapides à contourner ou à cracker.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Profils et sessions</strong></p>

Prenez le temps de réfléchir à l'usage que vous comptez faire de votre machine. Si d'autres utilisateurs en auront l'accès, utilisez des profils, c'est-à-dire des comptes personnels qui permettront d'une part à tous les utilisateurs d'organiser leurs sessions comme ils l'entendent, et d'autre part leur éviteront d'accéder aux données des uns et des autres.

</div>

Même si vous êtes seul-e à utiliser votre machine, créez au moins deux comptes&nbsp;: un compte disposant des droits d'administration et un autre dédié à l'utilisation (un [profil utilisateur](https://fr.wikipedia.org/wiki/Profil_utilisateur)). Il sera ainsi plus difficile pour un programme malveillant d'installer des fichiers visant à modifier le système. Cette recommandation vaut en particulier pour les possesseurs de Windows. En effet, sous GNU/Linux cette précaution élémentaire est déjà comprise dans le quotidien&nbsp;: un profil d'administration est toujours nécessaire pour installer un logiciel ou modifier un «&nbsp;fichier système&nbsp;». Notez aussi que le navigateur Firefox dispose d'une fonction dédiée à la gestion de profils.

### Virus et antivirus

Un logiciel antivirus est conçu pour identifier et neutraliser des programmes dont le comportement est suspect. En réalité, les virus informatiques ne sont qu'une catégorie de *[logiciels malveillants](https://fr.wikipedia.org/wiki/Logiciel_malveillant)*. Cette catégorie regroupe&nbsp;:

* *les virus*&nbsp;: ils se répliquent et se propagent à l'aide de fichiers «&nbsp;hôtes&nbsp;» (par exemple, un document texte que l'on ouvre par habitude). Ils peuvent infecter l'amorçage de la machine, les fichiers, le fonctionnement des programmes déjà existants… Un comportement classique d'un virus bien fait consiste à s'installer et effectuer des tâches de manière dissimulée (son fonctionnement passe inaperçu aux yeux de l'utilisateur). Un virus peut ainsi chiffrer un disque dur, envoyer son contenu quelque part et demander une rançon.
* *les vers*&nbsp;: ils se propagent d'eux-mêmes de machine en machine, notamment à travers le réseau (courrier électronique, partage de fichier, etc.)
* *les chevaux de Troie*&nbsp;: ils utilisent des portes dérobées (ou en ouvrent selon les besoins). Par exemple le développeur d'un logiciel peut laisser une entrée discrète (*backdoor*) au programme pour surveiller l'usage du logiciel ou même en prendre le contrôle. Nombre d'entreprises pratiquent ce genre de chose, à commencer par les plus connues comme Microsoft[^17].



#### Fonctionnement

Les logiciels antivirus agissent donc à de multiples niveaux sur la machine. Ils disposent habituellement de deux «&nbsp;grandes fonctionnalités&nbsp;»&nbsp;: les tâches de *firewall*[^18] qui consistent à surveiller les activités réseaux de la machine et les tâches d'anti-virus proprement dites, c'est à dire la surveillance des exécutions des programmes et des emplois de fichiers. Mais il leur faut généralement une base de donnée dans laquelle ils puisent des éléments de comparaison pour identifier les virus. C'est la raison pour laquelle un logiciel antivirus doit impérativement être mis à jour, c'est-à-dire lui permettre de télécharger, depuis le site du fournisseur, les *dictionnaires* qui permettent d'avoir toutes les informations utiles.

Une seconde stratégie utilisée par les logiciels antivirus, consiste non plus à identifier des logiciels malveillants mais à assister l'administrateur de la machine pour éviter les infections. Pour cela, il peut bloquer par défaut toute exécution de programme à l'exception de ceux pour lesquels l'administrateur a donné son accord.

Une troisième stratégie consiste en une méthode d'apprentissage. Les virus, à moins d'être extrêmement bien dissimulés, ont par définition un comportement suspect. Par exemple ils peuvent solliciter un programme en cours d'exécution ou tenter de supprimer / modifier des fichiers de manière inhabituelle. Le logiciel antivirus émet alors une alerte à l'intention de l'utilisateur.

Vous noterez que les deux dernières stratégies supposent une expertise de la part de l'utilisateur.  Bien souvent, c'est la principale faille du système&nbsp;: submergé par les alertes, ou ne sachant exactement quelle attitude employer (souvent parce que le manuel de l'antivirus n'est pas lu ou trop complexe), l'utilisateur clique un peu au hasard.

#### Que choisir&nbsp;?

Dans l'ensemble, un logiciel antivirus performant est nécessaire&nbsp;: c'est toujours à l'utilisateur qu'il revient de choisir. Voici quelques éléments de comparaison&nbsp;:

* l'efficacité&nbsp;: le logiciel dispose-t-il d'une base de données reconnue comme étant importante et fiable&nbsp;? Qui est son créateur et est-il reconnu&nbsp;? Il faut se renseigner&nbsp;;
* l'impact de l'antivirus sur les performances de la machine&nbsp;: souvent gourmands en ressources mémoire, car ils fonctionnent en permanence, les antivirus ne sont pas tous logés à la même enseigne&nbsp;;
* la complexité du fonctionnement et le paramétrage&nbsp;: les néophytes ne devraient pas utiliser des antivirus trop complexes, au risque de bénéficier d'une efficacité dégradée&nbsp;;
* l'interface&nbsp;: est-il facile d'utiliser le programme&nbsp;? les alertes sont-elles claires et assez compréhensibles pour ne pas perdre trop de temps&nbsp;?

Le choix d'un logiciel antivirus dépend donc de plusieurs paramètres. Mais nous en avons volontairement omis&nbsp;: le fait que le logiciel soit libre ou non. S'il n'est pas libre, vous devez faire confiance au fournisseur, sans toutefois oublier que, si le logiciel est payant et non-libre, son prix n'est pas forcément un gage d'efficacité, et si le logiciel est gratuit et non-libre, il faut se demander comment il est maintenu et par quels moyens.

Comme nous le répétons souvent&nbsp;: l'un des avantages des logiciels libres, c'est que le développement est communautaire. Un logiciel libre qui dispose d'une grande communauté, est un logiciel réputé efficace. C'est le cas de [Clamav](http://www.clamav.net/), un logiciel qui dispose, grâce aux remontées de la communauté des utilisateurs, d'une des plus grandes bases de données de signatures virales, c'est-à-dire autant de moyens d'identifier un virus.

Clamav est d'abord un logiciel créé pour les systèmes de type Unix (GNU/Linux ou MacOS). En effet, les serveurs de courriels sont majoritairement installés sur de tels systèmes, il est dès lors important de protéger les utilisateurs, en particulier ceux sous Windows. Or, comme la base de données de Clamav est libre, d'autres anti-virus libres, destinés en particulier à Windows, peuvent en profiter&nbsp;:

* [MoonSecure Antivirus](http://moonsecure.net/df/)&nbsp;: il offre une protection en temps réel avec une interface simple (bien qu'un peu vieillotte), tout en profitant de la base de Clamav. On peut bien sûr scanner des fichiers et des disques à la demande&nbsp;;
* [Clamwin](http://www.clamwin.com/) assure l'analyse des fichiers et disques en profitant lui aussi de la base de Clamav par contre il n'offre pas de protection en temps réel. Cette dernière tâche est assurée par son binôme, [Clam Sentinel](http://clamsentinel.sourceforge.net/).

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Les anti-virus ne peuvent pas tout</strong></p>

L'usage seul de ces logiciels (libres ou non) n'est en aucun cas une garantie contre les logiciels malveillants. Dans tous les cas, le meilleur moyen de s'en prémunir consiste à adopter des pratiques&nbsp;: ne pas télécharger ni installer de logiciels depuis d'autres sites que les sites officiels, se méfier des applications gratuites et non-libres (et même si elles sont libres, renseignez-vous sur la communauté), ne vous rendez pas sur des sites douteux, n'ouvrez pas les pièces jointes à vos messages sans d'abord en identifier la nature et la provenance, etc.

</div>



### Logiciels malveillants et systèmes d'exploitation

Pour le résumer en quelques mots, un virus informatique exploite des failles de sécurité. Dans ce domaine, aucun système d'exploitation n'est invulnérable. Cependant,  tous les systèmes d'exploitation ne sont pas logés à la même enseigne.

Protéger un ordinateur sous Windows suppose une attention de tous les instants et requiert, en plus de bonnes pratiques d'usage, une méfiance quasi-systématique, y compris vis-à-vis du système lui-même qui dépend des stratégies de la firme Microsoft. On peut en dire autant de la part de MacOS et d'autres systèmes. Sur une page intitulée «&nbsp;[Le logiciel privateur est souvent malveillant](https://www.gnu.org/proprietary/proprietary.html)&nbsp;», le site de la Free Software Foundation recense des exemples de malveillance de la part des logiciels privateurs&nbsp;; on peut y faire une recherche selon le type de malveillance et le nom des firmes.

Ces griefs adressés aux systèmes d'exploitation propriétaires ne signifient pas pour autant que les systèmes libres comme GNU/Linux sont exempts de toute vulnérabilité.  La différence, c'est que profitant d'une transparence dans le développement communautaire, lorsqu'une faille est découverte, il faut très peu de temps pour la corriger et propager la correction. Par exemple, la vulnérabilité Heartbleed, qui concernait les clés de chiffrement sous Linux (risque de les voir récupérées par des personnes mal intentionnées), a été découverte en mars 2014, rendue publique le 7 avril et les correctifs étaient déjà disponibles.

On peut s'amuser à comparer Windows et GNU/Linux pour comprendre pourquoi le second est mieux protégé par défaut contre les malveillances&nbsp;:


| Windows                                  | GNU/Linux                             |
|:---------------------------------------- | -------------------------------------:|
| Il faut créer volontairement au moins un compte utilisateur pour éviter que toutes les sessions ne se fassent avec des droits d'administration. | Les droits d'administrateur et les droits d'utilisateur sont d'emblée différenciés&nbsp;: un utilisateur n'a pas les droits de modification des fichiers système. |
| Les installations domestiques de Windows sont similaires, elles différencient seulement en fonction de la version de Windows (votre Windows 7 ressemblera au Windows 7 de votre voisin). | Il existe une multitude de distributions GNU/Linux différentes, avec des arrangements très différents (deux installations de la même distribution peuvent être  configurées à tel point qu'elles ne se ressemblent plus). |
| Les fichiers exécutables peuvent être installés facilement (leur blocage n'est pas activé par défaut). | Par défaut, il faut déclarer les droits d'exécution d'un fichier avant de l'utiliser comme exécutable. |
| Les failles mettent parfois plusieurs mois à être corrigées. | Grande réactivité de la communauté, failles corrigées rapidement. |
| Le système est une boîte noire et n'est auditable que par les ayants-droits. | Le code source est ouvert, un maximum d'acteurs, y compris de haut niveau, peuvent identifier les failles. |
| Il existe un *store* (magasin) d'application mais l'installation de programmes est souvent anarchique&nbsp;: trop de téléchargements et d'installations se font en récupérant des fichiers exécutables sur des sites peu sûrs. | La plupart des utilisateurs utilisent les dépôts officiels de leur distribution GNU/Linux pour installer des programmes, mais les utilisateurs utilisent parfois des dépôts non-officiels sans en évaluer le risque. |

On voit que les risques sont finalement assez similaires, qu'on utilise Windows ou GNU/Linux&nbsp;: tout est une question de pratique, de savoir activer des fonctionnalités ou ne pas le faire, etc. Pour autant, à moins d'être obligé-e d'utiliser Windows, il vaut mieux employer une distribution GNU/Linux non pas pour être sûr d'être protégé-e contre les malveillances, mais pour avoir à disposition un système plus difficilement attaquable.

Une autre opportunité, non pour se prémunir des logiciels malveillants mais pour protéger ses données, consiste à les *chiffrer*. Aujourd'hui les systèmes d'exploitation proposent la possibilité de chiffrement dès l'installation&nbsp;: Windows avec Bitlocker, MacOS avec Filevault, les distributions GNU/Linux avec l'outil de partitionnement LVM qui propose une option de chiffrement, etc. Les outils ne manquent pas. Et à condition de bien comprendre ce que l'on fait et de ne pas perdre ses clés, chiffrer ses données est encore le meilleur moyen de se protéger à la fois contre les malveillances et contre les surveillances. Encore faut-il ne pas se contenter de chiffrer les données sur son disque, mais de considérer tout transfert d'information comme une faille potentielle.



## Chiffrer mes données

Le chiffrement des communications est d'abord un outil. Lorsque vous communiquez avec votre banque ou que vous effectuez un paiement en ligne, vous devez vous assurer que vos communications sont bel et bien chiffrées de manière à ne pas courir le risque de les voir interceptées et copiées. N'oubliez pas&nbsp;: ce que vous faites en ligne, les messages que vous envoyez comme les sites que vous visitez, tout cela est une affaire de copie de contenus sur des serveurs et sur votre ordinateur (voir la section Navigation). Par exemple, même si *a priori* vous pouvez faire confiance aux employés de votre fournisseur d'accès, vous ne pouvez pas leur faire suffisamment confiance pour leur confier dans un courriel écrit en clair votre numéro de carte bancaire ou des informations relatives à votre état de santé.

Ajoutons à cela que, depuis l'affaire Snowden, nous savons que des firmes et des États mettent en œuvre des pratiques de surveillance des communications des citoyens&nbsp;: que ces pratiques évoluent, qu'un gouvernement soit ou non assez totalitaire pour retenir contre vous des informations *a priori* anodines, que vous le souhaitiez ou non, il est important que les citoyens puissent avoir à leur disposition un moyen d'échanger des informations de manière secrète… cela s'appelle de l'intimité numérique, exactement comme lorsque vous fermez la porte de votre salle de bain. Pour avoir accès à un moyen de garder au secret certaines de vos données et de vos correspondances dans le monde numérique, et donc exercer pleinement votre droit à la vie privée, vous devez savoir comment chiffrer des informations. C'est l'objet des prochaines sections.

### Pretty Good Privacy

Avant de commencer les explications, allons droit au but. De cette manière, si les prochains paragraphes vous semblent laborieux, l'essentiel vous sera familier. 

Imaginez une boîte, avec une serrure, qui enferme vos informations. Chacun de vos correspondants peut placer quelque chose dans cette boîte grâce à une formule et vous seul avez la clé qui permet de l'ouvrir. Tel est, de manière simplifiée et imagée, la méthode de chiffrement que nous allons expliquer, sauf que nous n'allons parler que de clés. Accrochez-vous ça commence.

L'un des premiers outils de chiffrement accessible à tous se nomme Pretty Good Privacy (PGP). Ce programme fut créé par Philip Zimmermann. Son histoire débute dans les années 1980 et le combat qui fut mené consista essentiellement à rendre PGP légal. On comprend aisément qu'un gouvernement considère de manière plutôt négative le fait que des citoyens puissent avoir accès à un système qui, par des lois mathématiques, rend impossible la lecture des messages qu'ils s'envoient.

PGP appartient à la [PGP Corporation](http://www.pgp.com) or, en 1998, cette société a rompu avec le principe de livrer le code source pour assurer une révision par les pairs.  En 2002, le code source est de nouveau disponible, mais entre temps, PGP a donné lieu à un standard proposé par l'IETF (Internet Engineering Task Force) nommé Open-PGP et décrit dans la [RFC 2440](https://www.ietf.org/rfc/rfc2440.txt). Suivant ce standard, le système GnuPG (Gnu Privacy Guard) fut créé, entièrement libre et par ailleurs agréé par Philip Zimmermann.

PGP est un système de chiffrement à clé publique[^19]. Pour comprendre de quoi il s'agit, il faut comparer ce système à un système de chiffrement symétrique. Tout le monde connaît le chiffrement symétrique&nbsp;: il s'agit d'utiliser la même clé pour chiffrer et déchiffrer un message. Fred envoie un message à Katy et le chiffre avec une table de correspondance entre les lettres de l'alphabet et des symboles. Katy et Fred doivent avoir la même clé (la table de correspondance) pour pouvoir chiffrer et déchiffrer leurs messages. Tout le problème est de faire circuler cette table / clé de manière secrète entre Katy et Fred. 

Le chiffrement à clé publique (ou asymétrique) présente un avantage majeur sur le chiffrement symétrique&nbsp;: *seules circulent les clés publiques et le déchiffrement appartient au destinataire seulement*. En effet, au départ, les correspondants se créent chacun un couple de clé publique et clé privée. Si les clés publiques peuvent être connues et utilisées par n'importe qui, les messages chiffrés avec une clé publique ne peuvent être déchiffrés qu'avec la clé privée qui lui correspond. Le problème de la circulation des clés de chiffrement est résolu.



Le cas de PGP est particulier dans ce domaine, puisqu'il ne se contente pas de chiffrer avec une clé publique. Une autre clé entre en jeu&nbsp;: la clé de session. Lorsque je chiffre un texte avec PGP, je le chiffre avec une clé de session générée sur le moment et de manière aléatoire. Ensuite, cette clé de session et le message chiffré  sont compressés ensemble et chiffrés avec la clé publique du destinataire. Le destinataire, quant à lui, récupère la clé de session grâce à sa clé privée et peut alors déchiffrer mon message. 

Pourquoi, en quelque sorte, ce chiffrement en deux temps&nbsp;? En fait, ce que le destinataire déchiffre en premier, c'est l'en-tête du message, de manière à l'authentifier. Ensuite seulement il déchiffre le message grâce à la clé de session récupérée. Cela rend PGP très sûr&nbsp;: chaque message possède sa propre clé et chaque clé de message doit correspondre à la clé privée du destinataire. Si je veux casser PGP, il faut que je casse à chaque fois le couple clé privée / clé de session. 

Par ailleurs, une autre fonction de PGP est l'authentification de l'expéditeur (la signature). Pour chaque message envoyé, PGP applique une fonction de hachage avec la clé privée de l'expéditeur&nbsp;: c'est le calcul d'une empreinte unique (un sceau) qui est jointe au texte et qui fait que, lorsque je reçois le message, je peux comparer cette empreinte avec celle que je calcule, cette fois, avec la clé publique de l'expéditeur. Je suis alors certain qu'il vient de mon correspondant.

Ajoutons à cela l'avantage de la compression. Un texte, aussi chiffré soit-il, peut toujours présenter des régularités. On peut par exemple en tirer des statistiques sur l'emploi de tels caractères et en inférer du sens. Souvenez-vous des magazines de votre enfance où vous deviez déchiffrer un message en trouvant les lettres des mots selon leurs fréquences et leurs probabilités. La compression d'un message permet de briser cette «&nbsp;logique&nbsp;» et ajoute un surplus de sécurité.

![Fonctionnement de PGP](images/pgpfonctionnement.png)


Dans les faits, si l'explication de PGP n'est pas évidente, son utilisation est assez simple. On peut se contenter de cette affirmation&nbsp;: chaque correspondant possède une clé privée et une clé publique&nbsp;; les clés publiques sont diffusées et tous les messages chiffrés à l'aide de la clé publique d'un correspondant ne pourront être déchiffrés qu'avec la clé privée de ce correspondant. Dans l'illustration, Fred envoie un message chiffré à l'aide de la clé publique (K) de Katy. Cette dernière peut alors déchiffrer le message avec sa clé privée (K'). Pour lui répondre, Katy utilisera la clé publique de Fred et ce dernier pourra déchiffrer à l'aide de sa clé privée à lui. Ce que fait PGP en sous-main (hachage, empreinte et clé de session) peut parfaitement être invisible pour l'utilisateur.


![Fred envoie un message avec la clé publique de Katy, cette dernière déchiffre le message avec sa clé privée](images/FredEtKaty_pgp.png)




### À quoi bon ?

Chiffrer un contenu, cela revient à utiliser une clé de chiffrement pour faire deux choses&nbsp;: rendre impossible la compréhension d'une information et l'authentifier. En effet, une clé de chiffrement peut être utilisée à la fois pour chiffrer tout un message et pour le signer. Une telle clé est donc à la fois un outil servant le secret des communications mais aussi une solution permettant d'instaurer un système de confiance entre l'émetteur et le récepteur.

Générer soi-même ses clés et envoyer les clés publiques à des correspondants suppose de maîtriser les flux d'informations. Recevoir un message chiffré ne signifie pas pour autant que l'on puisse faire confiance à l'émetteur. Si je reçois un message chiffré de la part d'un correspondant inconnu qui utilise ma clé publique pour le faire, je n'ai pas d'autre moyen que de faire connaissance avec ce nouveau correspondant pour m'assurer de son identité et faire confiance à l'avenir aux futurs messages qu'il m'enverra. De même, le contenu d'un message chiffré n'est pas forcément doté de bonnes intentions. Il faut donc bien séparer plusieurs notions&nbsp;: le chiffrement, la sécurité et l'authentification.

Une solution consiste à faire reposer la confiance sur une [autorité de certification](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_certification). Il s'agit la plupart du temps de sociétés dûment contrôlées qui endossent le niveau de confiance en proposant des certificats. Par exemple, l'ANSSI tient à jour [sur son site](https://www.ssi.gouv.fr/administration/reglementation/confiance-numerique/le-reglement-eidas/liste-nationale-de-confiance/) la liste des prestataires de confiance contrôlés par l'État Français. De tels prestataires proposent leurs services afin de permettre d'établir des connexions de confiance sur  Internet (nous le verrons plus loin dans ce chapitre à propos de HTTPS) c'est-à-dire qu'ils *certifient* des clés de chiffrement. S'il est certifié par une autorité, l'interlocuteur qui m'envoie un contenu chiffré grâce à ma clé publique est *a priori* digne de confiance. Non seulement ce qu'il m'envoie est confidentiel mais, en plus, la forme engage la responsabilité d'un tiers de confiance. 

Bien sûr, tous les utilisateurs n'ont pas à acheter de tels services pour leurs besoins quotidiens. Il est possible d'instaurer une chaîne de confiance à partir du moment où les clés sont partagées de manière à ce que chaque correspondant sache à quoi s'en tenir. Un message circulant en clair sur le réseau sera toujours susceptible d'être lu par un tiers. S'il est chiffré, il peut toujours y avoir une usurpation dans la manipulation des clés. Qu'il y ai ou non l'intervention d'une certification, tous les contenus qui circulent sur un réseau sont par nature sujets au risque de piratage.

Loin d'annuler complètement ces risques, un chiffrement a l'avantage de renforcer significativement le niveau de confidentialité d'un message. Même si je signe moi-même ma clé de chiffrement, sans certification attestée par une autorité, et que je confie ma clé publique à mes correspondants, les messages chiffrés que je recevrai ou que j'enverrai auront au moins l'avantage de pas être compréhensibles par quelqu'un d'autre. En théorie, si tout le monde appliquait le chiffrement des contenus échangés sur Internet, le niveau de sécurité global serait largement multiplié, même si tout le monde n'utilise pas les services d'autorités de certification. Évidemment, chiffrer ses propres fichiers et dossiers, voire son disque dur tout entier, est une précaution qui permettrait de lutter efficacement contre tous les virus de type *ransomware*   (qui chiffrent les données pour lesquelles les pirates réclament de l'argent en échange de la clé). Ceci sans compter la possibilité de conserver en sécurité des données sensibles ou tout simplement personnelles (les copies de vos papiers d'identité, vos feuilles d'impôt, vos ordonnances médicales, etc.)

En d'autres termes, le chiffrement à l'aide de logiciels libres (en particulier GnuPG &ndash;&nbsp;GNU Privacy Guard, l'implémentation du standard OpenPGP) permet de mettre à disposition de tous un moyen d'exercer efficacement le secret des correspondances. Il importe de prendre conscience que cette possibilité est fragile et parfois menacée, alors qu'elle relève clairement de l'exercice du droit à la vie privée si cher aux pays démocratiques.

Pour utiliser des solutions de chiffrement, on peut faire confiance à un service de messagerie incluant d'emblée un système de chiffrement, on peut aussi gérer soi-même ses clés de chiffrement et utiliser des logiciels spécifiques. Tout repose sur deux approches complémentaires des outils&nbsp;: évaluer le degré de confiance et intégrer le chiffrement dans des pratiques quotidiennes.

![Utiliser ses clés avec discernement](images/cleparcoeur.png)

### Faire confiance

Nous avons vu dans le chapitre 3 que pour envoyer des courriels, beaucoup de services proposent des messageries en ligne, comme des *webmail*. Certains prestataires offrent de surcroît la possibilité de chiffrer les messages à l'aide d'un système de chiffrement asymétrique comme PGP.  

Or, nous avons vu aussi que pour chiffrer de cette manière, il faut générer deux clés, l'une privée (à mettre en lieu sûr) et l'autre publique (à distribuer). Dès lors, si de tels services proposent le chiffrement des contenus en un clic pour leurs utilisateurs, ils présentent l'avantage de largement simplifier l'usage du chiffrement, mais en retour il leur faut gagner la confiance des utilisateurs qui leur confient la gestion à la fois de la clé privée et de la clé publique. De ce point de vue, tous les services ne sont pas égaux, et certains sont bien moins sérieux que d'autres. Il est important d'en évaluer la portée. 

Nous pouvons citer [Protonmail](https://protonmail.com), un service de *webmail* gratuit (pour les fonctionnalités de base) construit sur des logiciels libres. Il propose un chiffrement dit «&nbsp;de bout en bout&nbsp;», c'est-à-dire que seuls les correspondants peuvent chiffrer et déchiffrer leurs messages, et l'hébergeur n'a aucun moyen de les lire. Conscientes du problème lié à la gestion des clés dont Protonmail endosse la responsabilité et la confidentialité, les équipes de Protonmail ont mis au point un système qui permet de chiffrer les clés privées sur les serveurs de manière à ce que seul l'utilisateur-propriétaire de la clé puisse l'utiliser. L'ensemble du dispositif de Protonmail fait l'objet d'audits réguliers, dans une transparence relative que peu de services du genre sont en mesure de fournir. Dès lors, si le choix revient toujours à savoir où placer le curseur de confiance, et si tous les utilisateurs sont loin d'être capables d'auditer un tel service, les efforts de transparence et la lecture des avis éclairés peuvent constituer de bons indices.

Un tiers peut donc gérer complètement les clés nécessaires à un chiffrement efficace. Le prix à payer en retour de cette facilité d'usage, c'est la confiance. Mais d'autres systèmes de communication permettent de concentrer chez l'utilisateur les outils du chiffrement tout en permettant d'automatiser l'usage à un point tel qu'il passe presque inaperçu. 

C'est le cas de l'application [Signal](https://signal.org/), une application mobile (il existe une version autonome pour ordinateur) qui permet les communications écrites, audio et vidéo. Signal utilise un protocole particulier et libre, justement nommé *Signal Protocol*, qui permet un chiffrement de bout en bout. Ce protocole est utilisé par d'autres applications connues, et son histoire atteste largement de son efficacité. Du point de vue de l'utilisation, le chiffrement s'effectue de manière totalement automatisée entre les correspondants. Du point de vue de l'utilisateur, au lieu d'avoir à évaluer la confiance au regard du niveau de transparence d'un hébergeur, il doit placer sa confiance dans la structure du logiciel et la communauté des développeurs de ce logiciel libre. 

Avec ces deux exemples, on peut conclure que la confiance en un logiciel de communication chiffrée ne s'attribue pas à la légère. Pour certains utilisateurs dans des contextes politiques ou sociaux dangereux, il peut même s'agir d'une question de vie ou de mort. Même si l'utilisateur n'est ni développeur ni spécialiste de la sécurité informatique, au moins trois questions complémentaires se posent&nbsp;:

1. le dispositif repose-t-il sur un ou plusieurs logiciels ou protocoles libres&nbsp;? dans ce cas, les évaluations et les qualifications sont publiquement disponibles&nbsp;;
2. le développement ou le service hébergé sont-ils transparents vis-à-vis des utilisateurs&nbsp;? dans ce cas, il s'agit de savoir si l'on peut adhérer aux principes et aux intentions des prestataires ou de la communauté de développement&nbsp;; 
3. existe-t-il suffisamment de documentation, assez vulgarisée et claire sur les fonctionnalités des dispositifs et leurs évaluations&nbsp;? dans ce cas, c'est un gage d'honnêteté.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>La surveillance est une industrie</strong></p>
<p>Un récent [rapport de Cracked Lab](https://framablog.org/2017/10/25/comment-les-entreprises-surveillent-notre-quotidien/), intitulé *Corporate surveillance in Everyday Life* (2017) a examiné les pratiques de l'industrie de l'exploitation des *big datas*. Il en ressort que si le pistage et le profilage des individus est une activité extrêmement lucrative, elle profite aussi de développements technologiques spectaculaires et devient un instrument de contrôle social. Chiffrer ses données, c'est tâcher de laisser le moins de traces possible de son quotidien mais c'est aussi protéger la vie privée de tous.<p>
</div>

Quels que soient ses choix en matière de communication, il est nécessaire de savoir chiffrer par soi-même des informations, à commencer par des contenus qui n'ont pas forcément vocation à être échangés. En effet, un service aussi efficace soit-il peut toujours fermer ses portes de manière plus ou moins contrainte (ce fut le cas du service de courriel [Lavabit](https://fr.wikipedia.org/wiki/Lavabit) en 2013), et un logiciel «&nbsp;clés en mains&nbsp;» peut toujours manquer de développement et devenir obsolète, ce qui est vrai aussi pour les protocoles. Là encore, c'est vers des dispositifs comme GnuPG (OpenPGP) que nous devons nous tourner. Nous allons voir que dans l'usage quotidien du chiffrement asymétrique, il n'y a rien de bien complexe, et que grâce à quelques assistants bien faits, chiffrer est réellement à la portée de tous.



### Le chiffrement par l'exemple

Concrètement, lorsque je chiffre un contenu, qu'est-ce que je demande à la machine&nbsp;? Après avoir installé GnuPG qui me permet d'utiliser un standard de chiffrement PGP, je lui demande&nbsp;:

1. de faire un calcul sur les informations à chiffrer et utiliser une clé publique pour cela (la mienne ou celle d'un destinataire),
2. de produire un résultat (des données) illisible sans la clé,
3. de signer ce résultat, toujours à l'aide de ma clé, c'est-à-dire créer une empreinte à chiffrer pour produire une signature dont l'authentification sera possible avec ma clé ou celle d'un destinataire (cette signature permet aussi de vérifier l'intégrité du contenu chiffré et savoir s'il a été modifié).

Pour accomplir ces opérations, votre machine a besoin d'un programme qui lui permet d'effectuer les calculs et d'utiliser un protocole de chiffrement. Pour utiliser OpenPGP, la première étape consiste à se procurer le logiciel GnuPG[^20]. Il est disponible pour la plupart des systèmes d'exploitation, y compris pour des appareils mobiles, depuis le site officiel [gnupg.org](https://www.gnupg.org/). Téléchargez et installez.


Dans un second temps, vous pouvez vous pencher sur le [manuel de GnuPG](https://www.gnupg.org/gph/fr/manual.html). Mais sans toutefois vous formaliser&nbsp;: rassurez-vous, un usage classique n'est pas aussi complexe qu'au premier abord.

#### Ne pas perdre ses clés et savoir s'en servir

De la génération de clés au chiffrement de contenus, tout cela est faisable à la ligne de commande sous n'importe quel système d'exploitation. Cependant, à moins d'être très à l'aise avec le Terminal, où pour un apprentissage pédagogique, utiliser la ligne de commande n'est pas toujours ce qu'il y a de plus attirant. C'est pourquoi des interfaces graphiques ont été créés, qui permettent d'utiliser GnuPG, de gérer des clés (les vôtres et celles des destinataires) et d'effectuer des opérations de chiffrement de manière assez intuitive.

On peut prendre l'exemple de GPA ([GNU Privacy Assistant](https://www.gnupg.org/)). Complémentaire à GnuPG, GPA est un assistant de gestion de clés de chiffrement simple à l'utilisation et qui permet d'amorcer un apprentissage rapide.

Sous le système d'exploitation GNU/Linux il suffit d'installer GnuPG puis GPA depuis les dépôts de la distribution. Il y a d'autres interfaces graphiques proposées à l'usage, il suffit de faire son choix.

Sous Windows, on peut trouver un assortiment très intéressant&nbsp;: [GPG4Win](https://www.gpg4win.org/). Il est maintenu par une administration allemande, l'Office fédéral de la sécurité des technologies de l’information (*Bundesamt für Sicherheit in der Informationstechnik*). Il s'agit d'une suite logicielle (un choix est proposé lors de l'installation) contenant notamment&nbsp;:  

- GnuPG&nbsp;: à installer si ce n'est pas déjà fait,
- GPA (GNU Privacy Assistant)&nbsp;: le gestionnaire de clés&hellip;
- Kleopatra&nbsp;: un autre assistant gestionnaire de clés qui permet notamment d'interagir avec le plugin GpgOL,
- GpgOL&nbsp;: un plugin qui s'installe dans Outlook et permet de signer, chiffrer et déchiffrer des courriels (à la manière d'Enigmail pour Thunderbird, dont nous parlerons plus tard).

Le conseil est d'abord d'installer GPA et faire ses premières armes avec. Ensuite, les usages de Kleopatra et éventuellement le plugin pour Outlook vous seront facilement accessibles.

Dès l'ouverture de GPA, si vous n'avez pas encore de couple de clé privée / publique, le logiciel vous en proposera la création. Choisissez aussi de sauvegarder le jeu de manière à ce qu'à la fin du processus vous puissiez le stocker pour aussitôt le déplacer sur un support externe et le mettre en sûreté. Il est possible de lancer la procédure ultérieurement depuis le menu `Clé > Nouvelle clé`.

![Génération de clé avec GPA](images/gnuprivacyassistant1.png)



Remplissez avec application les champs qui vous sont proposés, fenêtre après fenêtre&nbsp;:

- choisissez une adresse courriel dont vous êtes sûr de la validité (si vous voulez que cette clé puisse servir pour que l'on vous envoie des messages),
- choisissez votre nom ou un pseudonyme,
- choisissez une durée de validité (au-delà de laquelle la clé ne sera plus valable) sauf si vous êtes absolument sûr de vouloir utiliser indéfiniment la même clé,
- choisissez un mot de passe fort et surtout ne l'oubliez pas&nbsp;!

Toutes ces informations seront incluses dans le jeu de clés qui, une fois créé sera lui-même chiffré et stocké. Il est très important d'exporter ce nouveau jeu de manière à le stocker dans un endroit sûr. Plus tard, lorsque vous serez plus à l'aise, vous pourrez générer des certificats de révocation et mettre des dates limites de validité sur vos clés.

Que faire ensuite avec GPA&nbsp;? facile&nbsp;! GPA propose un «&nbsp;presse papier&nbsp;», un petit éditeur de texte, qui vous permet d'écrire (ou coller) du contenu à chiffrer. De même, vous pouvez aussi ouvrir le gestionnaire de fichier pour choisir un fichier à chiffrer. GPA vous assiste alors&nbsp;: il suffit de choisir la clé publique avec laquelle vous désirez chiffrer le contenu et entrer le mot de passe de votre clé privée. Dans le cas d'un tampon de texte, GPA vous livre directement un contenu chiffré à copier et coller (par exemple dans un courriel) et dans le cas d'un fichier, GPA produit une copie chiffrée du fichier (qui se termine par l'extension `.gpg`).


<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Un serveur de clés</strong></p>

<p>Si vous devez leur envoyer des contenus chiffrés, vous devez récupérer les clés publiques de vos correspondants. Soit vous leur demandez de vous les fournir en mains propres (sur une clé USB, par exemple) soit vous utilisez les services d'un serveur de clés où vous aussi vous entrerez vos clés publiques.<p>

<p>Voici un exemple de serveur de clés connu, hébergé par le MIT&nbsp;: <a href="http://pgp.mit.edu/">MIT PGP Public Key Server</a>.</p>

<p>Pour envoyer une clé vers un serveur, il vous faut d'abord exporter votre clé. La plupart des serveurs de clés publiques possèdent une interface pour enregistrer les clés publiques des utilisateurs. Vous pouvez toujours ajouter d'autres clés, mais en général, assurez-vous que votre clé est bien celle que vous utiliserez pendant longtemps ou bien pensez à la révoquer.</p>
</div>


Vous avez compris le principe&nbsp;? et bien tous les assistants au chiffrement fonctionnent peu ou prou de la même manière. Comme GPA, il peuvent rechercher des clés sur un serveur de clés (avec les identifiants de clés), importer et exporter des clés, proposer une interface de chiffrement, etc.


#### Chiffrer mes courriels

Certains assistants s'intègrent parfaitement dans des clients de courriel. C'est le cas de l'extension [Enigmail](https://addons.mozilla.org/fr/thunderbird/addon/enigmail/) pour Thunderbird, que l'on installe depuis le catalogue d'extensions. Le [tutoriel (en français)](https://ssd.eff.org/fr/module/pgp-sous-windows-le-ba-ba) présent sur le site *Surveillance Self Defense* de l'Electronic Frontier Fundation s'adresse aux utilisateurs de MSWindows, et traite d'une installation fraîche de GnuPG, Thunderbird et Enigmail. Pour les utilisateurs de GNU/Linux ou MacOS, les démarches sont similaires.

Pour résumer, Enigmail vous permet de configurer Thunderbird de manière à utiliser à la demande le chiffrement de vos courriels. Il permet, tout comme GPA, de créer des clés, stocker, importer, etc.

Avec votre téléphone portable sous Android, l'application [OpenKeyCHain](https://www.openkeychain.org/) vous permettra d'utiliser PGP tout aussi facilement que les logiciels cités ci-dessus. Le client de courriel K9-Mail (cf. chapitre 3) l'utilise par défaut, à l'image du couple Enigmail-Thunderbird. Mais OpenKeyCHain peut être utilisé indépendamment, y compris pour chiffrer des fichiers.

Enfin, puisque nous en avons parlé précédemment, le couple Kleopatra (un assistant à la manière de GPA) et GPGOL (une extension pour le client de courriel Outlook) sont inclus dans la suite Gpg4WIN. Les utilisateurs d'Outlook peuvent donc utiliser GnuPG sans problème avec ces deux outils.


![Il ne faut jamais oublier que la sécurité informatique ne protège pas toujours de la criminalité organisée](images/securite_gege.png)



## Les mots de passe

Pourquoi utiliser des mots de passe&nbsp;? Alors que nous pouvons chiffrer nos données et nos messages avec des clés, les mots de passe servent à protéger l'accès aux dispositifs, c'est-à-dire ouvrir des sessions. Ainsi, vous avez un mot de passe pour accéder à votre messagerie, un mot de passe pour empêcher d'autres personnes d'utiliser votre ordinateur (à moins d'utiliser une session «&nbsp;invité&nbsp;»), un mot de passe pour chaque service web que vous utilisez, etc.

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>La complexité d'un mot de passe</strong></p>

Un «&nbsp;bon mot de passe&nbsp;» n'est pas une notion évidente. D'une part il doit être mémorisable, et d'autre part il faut y attacher un intérêt relatif à l'importance des données que l'on souhaite protéger. Ainsi certaines personnes jugent secondaire la complexité du mot de passe pour deux raisons&nbsp;: parce qu'elles craignent de l'oublier et parce qu'elles pensent que leurs données numériques n'ont aucune valeur (ce qui est fondamentalement faux).
</div>

Il y a plusieurs manières de pirater un mot de passe. La première consiste à tester des combinaisons une par une. On appelle cette méthode l'attaque par *force brute*. Elle nécessite un logiciel permettant d'automatiser très rapidement le traitement et les multiples essais. Si votre mot de passe est un mot présent dans un dictionnaire courant, de n'importe quelle langue, l'attaque durera quelques secondes. Plus longtemps si vous avez mis des chiffres. Par contre, plus votre mot de passe sera long et complexe, plus il faudra de temps pour le découvrir et, au-delà d'une certaine limite, la procédure ne peut aboutir compte tenu des possibilités techniques actuelles (sauf pour les récentes avancées des ordinateurs quantiques). Par exemple, 

```
UnmanchotvolantdansledesertduNevadalesoirdeNoel
```

 sera plus difficile à craquer que `manchot25`. Mais pas impossible.

Une autre méthode pour découvrir un mot de passe est dite par «&nbsp;ingénierie sociale&nbsp;». Elle consiste à établir le profil d'un utilisateur en fonction des informations qu'il a divulguées sur le web, notamment sur des réseaux sociaux. Ainsi, son nom, sa date de naissance, le nom de son chien, son adresse, etc. constitueront un ensemble de caractères pouvant être utilisés en priorité pour une attaque en force brute ou, plus simplement, pour élaborer des essais logiquement déduits par le pirate. 

Par exemple&nbsp;: Monsieur Dupont a assisté à la naissance de son fils Kévin le 12/06/2003. Un mot passe probable pourrait être `kevin2003`.


### Créer et utiliser les mots de passe

Il est conseillé de ne pas se contenter d'un simple mot composé uniquement de caractères alphabétiques `[a-z]`. Il vaut mieux utiliser une combinaison comprenant des caractères alphanumériques ```[a-z, 0-9]```, des majuscules `[A-Z]` et des caractères spéciaux `[!,:@, etc.]`, le tout sur une longueur minimum de 10 caractères. L'idéal est de retenir une phrase de passe, qui excédera 10 caractères (des quatre types), et sera par conséquent plus facile à retenir. 

Pour construire une phrase de passe, il faut aussi mémoriser la méthode de sa construction, sous peine de définitivement l'oublier. Plusieurs méthodes pourront vous aider&nbsp;:

* prenez une phrase et remplacez certaines lettres par d'autres caractères (et utilisez les 4 types de caractères mentionnés ci-dessus),
* utilisez la phonétique, par exemple : `GHT4dvdchez@t1t1!` (j'ai acheté 4 dvd chez @Tintin !),
* utilisez un générateur de mot de passe (le logiciel Keepass le permet, cf. plus bas).

Prenez quelques minutes pour vous inventer une méthode que vous n'oublierez pas. Un bien petit sacrifice pour la sécurité de vos données…

Dans tous les cas, respectez impérativement ces règles&nbsp;:

* utilisez un mot de passe différent pour chaque service auquel vous êtes inscrit-e,
* nettoyez le cache du navigateur après chaque utilisation et, par défaut, ne mémorisez pas le mot de passe dans votre navigateur,
* n'écrivez jamais votre mot de passe, même si on vous le demande : l'administrateur d'un site n'a jamais besoin de votre mot de passe pour effectuer des opérations sur le site,
* changez régulièrement de mot de passe.


### Comment stocker mes mots de passe

En matière de pratiques comme de sécurité informatique, un adage connu affirme que le problème se situe souvent entre la chaise et le clavier. En effet, on ne compte plus les personnes qui stockent leurs mots de passe à l'aide de papiers collés sur l'écran ou sous le clavier. Il ne faut pas oublier beaucoup d'intrusions informatiques se font grâce à un accès physique aux machines, en particulier lorsque des ordinateurs se trouvent dans des endroits publics, ou même dans un bureau, et bien sûr en cas de vol. Il faut donc rester vigilant. 

Heureusement d'autres moyens sont à votre disposition&nbsp;: les logiciels gestionnaires de mots de passe. Attention&nbsp;: beaucoup de solutions logicielles existent sur le marché, il faut savoir évaluer leur degré de confiance. Il existe trois types de solutions&nbsp;:

1. des logiciels / applications propriétaires. À moins que votre entreprise en ait fait l'audit et vous oblige à en utiliser une, ne faites pas confiance à ces logiciels tant que vous n'en n'avez pas l'expertise&nbsp;;
2. des services en ligne. Là aussi, vous devez non seulement faire confiance aux technologies employées, mais elles sont de plus, et par définition, vulnérables à de nombreuses attaques (quel pirate ne rêve pas de s'attaquer à un site qui prétend regrouper  les données d'accès de milliers d'utilisateurs&nbsp;?)&nbsp;;
3. des solutions logicielles libres, qui non seulement font l'objet d'audits permanents de la part des utilisateurs (du moins ceux qui en ont l'expertise), mais qui, de surcroît jouent la transparence en publiant leur code source. 

Pour cette troisième catégorie, il est aussi plus facile de se renseigner sur les audits. Ainsi, l'un des logiciels les plus plébiscités dans ce domaine se nomme [Keepass](http://keepass.info/) (Keepass Password Safe). C'est un logiciel libre qui a de plus été [passé au crible](https://www.ssi.gouv.fr/entreprise/certification_cspn/keepass-version-2-10-portable/) par l'Agence Nationale de la Sécurité des Systèmes d'Information (ANSSI) et bénéficie d'une Certification de Sécurité de Premier Niveau (CSPN). La CNIL [mentionne Keepass](https://www.cnil.fr/fr/construire-un-mot-de-passe-sur-et-gerer-la-liste-de-ses-codes-dacces) sur une page consacrée aux mots de passe dont vous pouvez bénéficier de la lecture.

Keepass a d'abord été développé pour Windows. Dans sa version GNU/Linux, il se nomme [KeepassX](https://www.keepassx.org/), un portage de Keepass. L'utilisation est très simple. Il s'agit d'une base de données dotée d'une interface pour remplir différents champs (titre, nom d'utilisateur, mot de passe, etc.). Il est possible de spécifier une date d'expiration pour chaque entrée.

![Entrée de données dans Keepass](images/keepassx2.png)

La base de données est sauvegardée dans un fichier d'extension `.kdbx` et dotée d'une phrase de passe. Cette dernière est alors suffisante pour ouvrir la base et récupérer ses mots de passe. Cela permet à l'utilisateur de n'avoir à retenir qu'un seul mot de passe pour tous les autres. Bien sûr ce mot de passe devra être bien complexe et le fichier de la base stocké dans un endroit difficile d'accès.

![Interface de Keepass](images/keepassx1.png)

Beaucoup d'extensions (plugins) sont disponibles. Elles permettent par exemple d'interfacer Keepass avec son navigateur, de synchroniser Keepass avec d'autres appareils comme un smartphone, ou encore de synchroniser la base de données avec un stockage en ligne (déconseillé sauf pour un usage peu sensible).

Du côté des appareils mobiles, l'application [Keepassdroid](http://www.keepassdroid.com/) (mentionnée sur le [site PrismBreak](https://prism-break.org/fr/projects/keepassdroid/)) est un port de Keepass sur plate-forme Android et permet donc d'utiliser des fichiers `.kdbx`. Avec un peu de pratique, il est alors possible d'ouvrir la même base de donnée sur votre mobile et sur ordinateur, ce qui vous permet de la transporter avec vous. Toutefois, votre prudence est de mise&nbsp;: les appareils mobiles sont des dispositifs très sensibles au niveau de la sécurité et du chiffrement.

## Surfer en sécurité

Comme nous l'avons vu avec PGP, il est important de bien comprendre que sur le réseau, lorsque deux machines communiquent entre elles, il faut leur demander explicitement de le faire de manière chiffrée. En fait, lorsque vous surfez sur le web, vous utilisez des protocoles qui permettent par exemple à votre navigateur d'envoyer et recevoir des informations et les afficher. C'est le cas d'usage du HTTP (HyperText Transfert Protocol). Or, selon les besoins, surtout lorsque vous consultez un site dont les données sont sensibles (votre compte en banque, vos données de santé, votre messagerie en ligne, etc.) il est important d'ajouter un protocole de sécurité en plus&nbsp;: SSL (Secure Socket Layer).

### Le HTTPS&nbsp;: pourquoi&nbsp;?

SSL est une couche (layer) supplémentaire aux protocoles habituels qui permet d'amorcer une session sécurisée de transmission entre deux machines. Lorsque vous surfez et que vous voyez apparaître l'occurrence `https` au lieu de l'habituel `http` dans la barre d'adresse, c'est que la connexion s'établit, en plus, sur une base SSL. Très rapide et pratiquement invisible aux yeux de l'internaute, une connexion SSL se déroule en deux temps (deux protocoles)&nbsp;:

* une phase de négociation (SSL Handshake protocol)&nbsp;: les deux machines négocient des clés de chiffrement et s'accordent sur les protocoles d'échange,
* la phase d'échange (SSL Record protocol) &nbsp;: les deux machines communiquent et contrôlent les échanges.

Comme dans le cas de l'utilisation de PGP, le chiffrement est asymétrique (échange de clés) et utilise un système de signature visant à contrôler l'intégrité des informations de l'émetteur au récepteur (s'assurer que personne n'a intercepté l'information pour la renvoyer modifiée).

Un concept très important dans le cas de connexion SSL, c'est le certificat. Lors de la phase de négociation, les deux machines échangent des certificats, c'est-à-dire des pièces d'identité. Lorsque vous vous connectez à votre banque, que vous ayez un certificat ou non importe peu&nbsp;: l'essentiel est que votre banque, elle, en ait un qui puisse l'authentifier de manière à être sûr-e que c'est bien sur le site de votre banque que vous vous trouvez et avec qui vous allez échanger des informations.

Là encore, ce n'est pas l'utilisateur qui, manuellement, doit s'assurer de l'authenticité du certificat émis par le correspondant. En fait, chaque navigateur dispose d'une liste d'autorités (des PKI, Public Key Infrastructure) auxquelles il s'adresse et qui vont endosser cette authentification. Ces autorités sont des organismes agréés (sociétés de droit privé ou institutions publiques) qui disposent d'un ensemble de dispositifs informatiques et humains de certification.

![Exemple de certification authentifiée sur le site Wikipédia](images/firefox_certif.png)

Si le navigateur ne parvient pas à faire le lien entre une autorité et le certificat reçu, il prévient l'utilisateur, notamment avec un jeu d'icônes. Dans le cas du navigateur [Firefox ](https://www.mozilla.org/fr/firefox/desktop/) les icônes en forme de cadenas correspondent à [plusieurs cas de figure](https://support.mozilla.org/fr/kb/comment-savoir-si-ma-connexion-est-securisee)&nbsp;:

* une simple information qui prévient que le site n'est pas certifié (mais comme n'y a pas lieu d'y entrer des informations, l'absence de formulaire, etc. il n'y a pas d'alerte)&nbsp;;
* une alerte signifiant que le site n'est pas authentifié, que la communication n'est  pas chiffrée, et que tout ce qui pourra y être communiqué par l'utilisateur risque d'être intercepté&nbsp;;
* un avertissement indiquant que le site, bien qu'authentifié, contient des éléments qui ne sont pas sécurisés et que la communication n'est que partiellement chiffrée&nbsp;;
* une indication qui atteste que la communication est entièrement chiffrée et le site certifié.

En d'autres termes, lorsque vous vous rendez sur un site où vous êtes censé entrer des informations, confidentielles ou non, ayez toujours un œil en haut à gauche de la fenêtre de votre navigateur&nbsp;!



### L'affaire des cookies

Dans le jargon informatique, un [cookie](http://www.catb.org/~esr/jargon/html/C/cookie.html) est un moyen de formaliser une transaction entre deux programmes, le plus souvent par un fichier qui fait office de jeton. Il s'agit d'un *témoin de connexion* qui atteste la connexion d'une machine à une autre. Les cookies existent depuis très longtemps dans l'histoire de l'informatique moderne mais l'omniprésence des réseaux a multiplié leur usage.

Concrètement, un cookie est un fichier texte stocké sur la machine de l'internaute (généralement dans un emplacement désigné et utilisé par le navigateur) et qui retourne des informations lorsque le serveur les lui demande. Par exemple, lorsque vous faites des achats sur une boutique en ligne, un cookie contenant des identifiants ou un code généré en fonction des identifiants de connexion, vous permet de gérer votre panier d'achat en renvoyant ce cookie qui vous identifie sur le serveur à chaque fois que vous y ajoutez quelque chose ou lorsque vous voulez payer.

Les cookies sont donc des dispositifs fort utiles. Ils sont là au départ pour permettre d'ouvrir et fermer une session d'utilisation d'un site web ou de tout autre service. Cependant, ils peuvent emmagasiner bien d'autres informations, notamment à des fins de marketing mais qui peuvent mettre en danger vos informations personnelles et votre confidentialité.

#### Cookies et sécurité

Un cookie n'identifie pas l'utilisateur derrière le clavier, mais le navigateur sur votre machine se connectant à un serveur distant. Fermer une session sur un site distant ne signifie donc pas forcément que le cookie qui vous permettait de vous identifier est nettoyé de ces informations. Si une personne utilise votre machine et votre navigateur après vous, il y a des chances qu'elle puisse accéder à des informations non souhaitées.

Voici un scénario probable&nbsp;: en prévision de la Saint Valentin, vous effectuez des achats en ligne pour faire une surprise à votre conjoint-e. Un cookie va alors stocker la valeur totale de vos achats. Une fois les achats effectués, votre conjoint-e utilise à son tour le navigateur pour se rendre avec la même intention sur le même site… et voit s'afficher le montant de votre dernière transaction.

Ce dernier scénario issue de la vie domestique peut prêter à sourire, mais les  conséquences ne sont pas toujours négligeables&nbsp;: certains cookies, ajoutés à votre historique de navigation, vous permettent même de retourner des informations d'identité pour ne pas avoir à saisir votre numéro de carte bleue lors des achats ultérieurs. Or, les autres utilisateurs de la machine (autorisés ou pas) ne sont pas toujours des personnes bien intentionnées…

Un autre exemple de vulnérabilité concerne l'interception de cookies. Un tiers peut en effet s'interposer entre votre navigateur et le serveur, ou bien ce dernier peut lui-même avoir été piraté. Dès lors, il est possible que votre session soit détournée et que toutes les informations que vous retournez avec les cookies soient récupérées. Ce détournement de session est une pratique qui peut être contrée par la protection SSL et le chiffrement de session, ce que nous avons vu précédemment à propos de HTTPS. Cependant, même si la connexion entre le serveur et le client est chiffrée, la durée d'existence d'un cookie dans votre navigateur peut parfois être de plusieurs jours et excéder la durée de la connexion, ce qui cause une réelle faille de sécurité pour vos données privées en fonction des sites sur lesquels vous vous rendez.



#### Les cookies et ma vie privée

La directive européenne du 25 novembre 2009 (2009/136/CE modifiant la directive 2002/22/CE &ndash;&nbsp;directive «&nbsp;service universel&nbsp;») indique&nbsp;:

> Les États membres garantissent que le stockage d'informations, ou l'obtention de l'accès à des informations déjà stockées, dans l'équipement terminal d'un abonné ou d'un utilisateur n'est permis qu'à condition que l'abonné ou l'utilisateur ait donné son accord, après avoir reçu, dans le respect de la directive 95/46/CE, une information claire et complète, entre autres sur les finalités du traitement.

On comprend que dans ces conditions l'usage des cookies devrait être réservé à l'amélioration des services et de l'expérience utilisateur. Cependant, dans la mesure où les cookies servent surtout à mesurer des parts d'audience et les comportements des utilisateurs, beaucoup de sites font en réalité appel à des sociétés spécialisées. Dans d'autres cas, des grands acteurs de services, tels Facebook, offrent grâce à leur notoriété de quoi augmenter l'envergure de leurs services en proposant à d'autres sites de les relayer. C'est le cas, par exemple, lorsque le site d'un quotidien permet de «&nbsp;liker&nbsp;» un article avec un bouton Facebook&nbsp;: cela permet d'augmenter l'audience du journal en relayant l'article sur Facebook et cela permet à Facebook de mesurer le comportement de ses utilisateurs pour leur offrir une «&nbsp;meilleure expérience&nbsp;» en leur proposant des contenus similaires.

Ces procédés qui consistent à user et abuser de cookies issus de tierces parties ouvrent de plus vastes questions sur la vie privée. Si je me rends sur un site, je ne m'attends pas forcément à ce qu'un tiers fasse le lien entre cette visite et un autre site. Un même émetteur est ainsi capable de recouper les informations issues de plusieurs sites sur lesquels se rend l'utilisateur. Ces analyses comportementales permettent d'inférer des profils à l'insu des utilisateurs. Cela fait des cookies non plus des instruments au service de l'expérience technique de l'utilisateur, mais de véritables mouchards. C'est pourquoi certains pays ont tenté d'adopter une législation stricte en la matière.


#### Surveiller les cookies que je reçois

Comme nous l'avons vu, si tous les cookies ne sont pas amicaux, le principe ne doit néanmoins pas être rejeté en bloc.  Une bonne pratique, qui ne nécessite pas de compétences particulières, consiste à nettoyer son navigateur une fois que la session est terminée. Pour ce faire, une fonctionnalité de votre navigateur vous permet de supprimer l'historique, cookies compris, de manière ponctuelle ou de manière systématique. N'hésitez pas à employer cet outil.

Une autre manière de surveiller les cookies, consiste à paramétrer son navigateur. Par défaut, un navigateur comme Firefox accepte les cookies, y compris les cookies de tierce partie. Il est donc possible de les bloquer systématiquement en se rendant dans Firefox&nbsp;: `Préférences > Vie privée`.

![Surveiller les cookies avec Firefox](images/firefox_vieprivee.png)


Une autre méthode consiste à surfer en utilisant des fenêtres de navigation privée (en cliquant sur le petit masque ou en faisant `CTRL+Maj+P`). Le mode de navigation privée [protège contre le pistage](https://support.mozilla.org/fr/kb/protection-contre-le-pistage-en-navigation-privee) et n'enregistre pas d'historique.


En 2014, l'Electronic Frontier Foundation (EFF) s'est penché sérieusement sur le problème des cookies. L'EFF est une organisation non gouvernementale et à but non lucratif, qui vise à défendre la liberté d'expression sur Internet. Son rôle se compose à la fois de veille, d'alerte et de pédagogie. Dans la mesure où l'usage déloyal des cookies menace nos intimités numériques, l'EFF a développé une extension pour Firefox et Chrome nommée [Privacy Badger](https://www.eff.org/fr/privacybadger). Cette extension, installable en un clic sur votre navigateur, permet de monitorer et bloquer les cookies. Son but général est à terme d'obliger les émetteurs à respecter la fonction *do not track* (DNT — une [fonction du navigateur](https://support.mozilla.org/fr/kb/comment-activer-option-ne-pas-pister) qui indique que l'utilisateur ne souhaite pas être pisté).

La [page Wikipédia consacrée à Privacy Badger](https://fr.wikipedia.org/wiki/Privacy_Badger) explique en quelques mots simples le fonctionnement de Privacy Badger&nbsp;:

> Privacy Badger ne fonctionne pas à partir d'une liste toute faite de sites à bloquer (liste noire) mais utilise un algorithme pour détecter dynamiquement des comportements de pistage par des sites tiers. Cette approche heuristique présente deux avantages&nbsp;: actualité (par la technique de l'apprentissage automatique la contre-mesure est toujours à jour), impartialité (pas de pressions tierces concernant le contenu d'une liste centralisée pré-établie).
> 
>Lors de la visite d’un site, le navigateur web charge automatiquement des contenus de différents sites tiers&nbsp;: Privacy Badger recense ces sites. Puis, si au cours des pérégrinations en ligne ultérieures, les mêmes sites semblent pister la navigation sans autorisation, alors Privacy Badger entre en action en demandant au navigateur de les bloquer. Et comme le navigateur ne charge plus rien en provenance de ces sites, ils ne peuvent plus pister l’internaute.

![Usage de Privacy Badger sur un site connu](images/screen_privacybadger.png)


## Et mon anonymat&nbsp;?

De manière synthétique, Eben Moglen[^21] exprime en une phrase tout le problème de nos intimités numériques sur Internet&nbsp;: «&nbsp;nous n'avons pas inventé l'anonymat lorsque nous avons inventé Internet&nbsp;»[^22]. En effet, aux fondements d'Internet, c'est le partage de l'information qui prime. La surveillance des utilisateurs n'est qu'une conséquence d'un usage déloyal des protocoles utilisés pour partager l'information.

Dès lors, l'anonymat prend une autre dimension. Il n'est plus seulement l'ignorance ou la dissimulation de l'identité. Car ce qui définit justement notre identité sur Internet, ce n'est pas notre présence administrative  c'est l'ensemble des comportements et des traces numériques qui sont analysées, recoupées, inférées, de manière à profiler notre identité et celle de nos correspondants. Ces usages sont devenus tellement automatiques que beaucoup d'internautes sont prêts à sacrifier une partie de leurs droits à l'image pour pouvoir utiliser un service gratuit (voyez les conditions d'utilisation de Facebook qui s'approprie vos données biométriques et pratique des analyses de reconnaissance faciale[^23]). Mais l'enjeu est encore plus large&nbsp;: la raison pour laquelle nous devons protéger nos accès aussi bien que notre identité numérique, c'est d'abord et avant tout pour protéger ceux avec qui on échange. Car tout l'intérêt de l'analyse des données, ce n'est pas le contenu de votre dernier message à votre ami à l'autre bout du pays, c'est la fréquence de ces échanges, les lieux et les durées, et ce sont ces données qui intéressent avant tout les firmes pour des raisons de marketing et les programmes de surveillance de certains États (qui impliquent ces mêmes firmes[^24]).

<div class="alert alert-info">
<p><img src="images/encarts/cloche.png" style="margin-right:10px;"><strong>Anonymat ou confidentialité ?</strong></p>

Attention toutefois à ne pas confondre l'anonymat et l'impossibilité d'être identifié. Quoi que vous fassiez sur Internet, il est impossible de masquer totalement un ou plusieurs éléments qui permettent de remonter à la source. Pour différencier les données, il faut bien fatalement que votre machine soit identifiée sur le réseau. À vous de faire en sorte que l'identité technique de votre machine soit distincte de votre identité administrative. On préfère donc parler de *confidentialité* sur Internet.

</div>



## Utiliser TOR

TOR est l'acronyme de The Onion Router. Il s'agit d'abord d'un réseau qui  fonctionne par nœuds qui «&nbsp;répercutent&nbsp;» les connexions des utilisateurs. Ainsi, tout comme les couches des pelures d'oignon, si on identifie un nœud par lequel l'utilisateur est passé, c'est à un autre nœud qu'il renvoie, etc. Le réseau TOR est donc décentralisé. Pour fonctionner, la liste des nœuds étant publique, il connecte les utilisateurs sur ces nœuds et construit des routes (un routage) qui leur permettent de surfer sur Internet mais en passant par différents nœuds à chaque fois. Le site visité ou tout autre acteur ne peut donc pas identifier la provenance exacte de l'utilisateur, à moins d'utiliser des outils spécifiques d'analyse en profondeur du réseau et vous rechercher activement.

Pour assurer cette confidentialité pour toutes sortes de connexion Internet, il faut configurer son système d'exploitation avec les données du réseau TOR. C'est une pratique plutôt fastidieuse. C'est pour cette raison qu'il existe [TAILS](https://tails.boum.org/) (The Amnesic Incognito Live System), une distribution GNU/Linux entièrement paramétrée pour cela. TAILS n'est pas censée obligatoirement s'installer sur votre disque dur à la place de votre système d'exploitation actuel. C'est d'abord une distribution «&nbsp;live&nbsp;», c'est-à-dire qu'elle s'installe sur une clé USB ou une carte SD et vous permet de disposer ainsi d'une session à la  demande, non seulement pour surfer, mais aussi pour disposer d'un panel d'outils dédiés au chiffrement.

De manière moins complexe, il existe un navigateur développé par le projet Tor, sur la base de Firefox&nbsp;: [Tor Browser Bundle](https://www.torproject.org/projects/torbrowser.html). Pour résumer, ce navigateur s'installe et s'utilise de la même manière qu'un navigateur classique, à la différence qu'il se connecte d'abord au réseau TOR et que vous pouvez configurer le niveau de sécurité avec un curseur. Si vous utilisez Windows, vous pouvez vous reporter à [cette page d'explication](https://ssd.eff.org/fr/module/guide-dutilisation-de-tor-pour-windows) pour installer et configurer Tor Browser. La seule condition pour utiliser Tor Browser correctement, c'est de toujours disposer de la dernière version à jour.


Pour conclure sur l'utilisation de TOR, il est important de préciser que cet outil n'est pas destiné à effectuer des opérations illicites. Il est d'abord construit de manière à respecter la confidentialité des utilisateurs. Il n'est donc pas forcément pertinent de l'utiliser à tout bout de champ, dans un délire paranoïaque permanent, même si dans certains pays son utilisation (couplée à d'autres dispositifs encore) est d'abord une question de liberté d'expression et de survie.  L'utilisation de TOR se fait à bon escient, car rien ne remplacera votre attention et votre bon sens.


[^17]: Par exemple, pour optimiser sa stratégie de propagation de Windows 10, Microsoft a en effet utilisé des portes dérobées sous Windows 7 et 8 en modifiant un avertissement indiquant que la mise à jour vers Windows 10 devait être faite, même si l'utilisateur l'avait préalablement refusée. Pour cela une porte dérobée a été utilisée à l'insu des utilisateurs. Voir Gregg Keizer, «&nbsp;[Microsoft sets stage for massive Windows 10 upgrade strategy](http://www.computerworld.com/article/3012278/microsoft-windows/microsoft-sets-stage-for-massive-windows-10-upgrade-strategy.html)&nbsp;», *ComputerWorld*, 07/12/2015.

[^18]: Littéralement un *pare-feu* : il consiste à mettre en place une politique de sécurité réseau. Il est toutefois préférable d'utiliser un *firewall* plus complet que les seules fonctions offertes par les logiciels anti-virus, même si elles couvrent les besoins immédiats.

[^19]: Pas vraiment, il est en fait hybride&nbsp;: comme on le verra plus loin, PGP utilise un couple clé privée / clé publique mais ajoute à cela l'empaquetage des messages avec leur clé de déchiffrement et ce sont ces clés que les clés publiques et privées permettent de récupérer pour déchiffrer les messages. 

[^20]: Vous pouvez aussi lire avec intérêt cet article de David Legrand, «&nbsp;[OpenPGP et GnuPG&nbsp;: 25 ans de chiffrement pour tous, ce qu'il faut savoir avant de s'y mettre](https://www.nextinpact.com/news/98509-openpgp-et-gnupg-25-ans-chiffrement-pour-tous-ce-quil-faut-savoir-avant-sy-mettre.htm)&nbsp;», *NextInpact*, 27/12/2016.


[^21]: [Eben Moglen](https://fr.wikipedia.org/wiki/Eben_Moglen) est professeur de droit et d'histoire du droit à l'université Columbia, président du Software Freedom Law Center, et avocat conseil à la Free Software Foundation.

[^22]: Voir cette conférence (traduction sous-titrée) de Eben Moglen, «&nbsp;[Why Freedom of Thought Requires Free Media and Why Free Media Require Free Technology](https://framatube.org/media/e-moglen-defendre-notre-liberte-de-penser-exige-de)&nbsp;», *Re:Publica*, Berlin, 02/05/2012.


[^23]: Pour plus d'information, voir les articles suivants. Tom Simonite, «&nbsp;[Facebook Creates Software That Matches Faces Almost as Well as You Do](https://www.technologyreview.com/s/525586/facebook-creates-software-that-matches-faces-almost-as-well-as-you-do/)&nbsp;», *MIT Technology Review*, 17/03/2014. Dino Grandoni, «&nbsp;[DeepFace, le nouveau système de reconnaissance faciale de Facebook qui fait froid dans le dos](http://www.huffingtonpost.fr/2014/03/20/deepface-reconnaissance-faciale-facebook_n_5000872.html)&nbsp;», *Le Huffington Post*, 20/03/2014. Dépêche AFP/Le Figaro, «&nbsp;[Facebook devant la justice pour sa technologie de reconnaissance faciale](http://www.lefigaro.fr/secteur/high-tech/2016/05/06/32001-20160506ARTFIG00087-facebook-devant-la-justice-pour-sa-technologie-de-reconnaissance-faciale.php)&nbsp;», *Le Figaro*, 06/05/2016.

[^24]: Si vous pensez ne rien avoir à cacher, vous pouvez regarder cette vidéo par Julien Vaubourg (Lorraine Data Network), «&nbsp;[Je n'ai rien à cacher](https://framatube.org/media/je-nai-rien-a-cacher)&nbsp;», séminaire MathC2+, à Inria Grand Est, 14/04/2015.



